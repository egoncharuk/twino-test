Twino test app

-- to prepare package

mvn package

-- to Init HSQL DB

java -jar target/twino-test-1.0-SNAPSHOT.jar db migrate config.yml

-- to Run server

java -jar target/twino-test-1.0-SNAPSHOT.jar server config.yml