package com.test.app.mappers;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.test.app.dao.LoanModel;
import com.test.app.dto.LoanDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class ObjectsMapper extends ConfigurableMapper {

    /**
     * Orika Mapper Facade.
     */
    private static final MapperFacade mapper;

    static {
        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

        mapperFactory.classMap(LoanModel.class, LoanDto.class)
                .mapNulls(true)
                .mapNullsInReverse(true)
                .byDefault()
                .register();

        mapper = mapperFactory.getMapperFacade();
    }

    public static MapperFacade mapper() {
        return mapper;
    }

}
