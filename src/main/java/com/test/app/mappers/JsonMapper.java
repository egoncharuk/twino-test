package com.test.app.mappers;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonMapper {

    private static final ObjectMapper jackson;

    static {
        jackson = new ObjectMapper();

        jackson.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        jackson.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        jackson.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        jackson.configure(DeserializationFeature.WRAP_EXCEPTIONS, false);
    }

    public static ObjectMapper getJsonMapper() {
        return jackson;
    }

}
