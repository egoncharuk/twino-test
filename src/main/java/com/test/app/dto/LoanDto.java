package com.test.app.dto;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

public class LoanDto {

    private int amount;

    private String term;

    private String name;

    private String surname;

    private String personalId;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoanDto)) return false;

        LoanDto loanDto = (LoanDto) o;

        if (amount != loanDto.amount) return false;
        if (term != null ? !term.equals(loanDto.term) : loanDto.term != null) return false;
        if (name != null ? !name.equals(loanDto.name) : loanDto.name != null) return false;
        if (surname != null ? !surname.equals(loanDto.surname) : loanDto.surname != null) return false;
        return !(personalId != null ? !personalId.equals(loanDto.personalId) : loanDto.personalId != null);

    }

    @Override
    public int hashCode() {
        int result = amount;
        result = 31 * result + (term != null ? term.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (personalId != null ? personalId.hashCode() : 0);
        return result;
    }
}
