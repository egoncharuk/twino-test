package com.test.app;

import com.test.app.configuration.AppConfiguration;
import com.test.app.dao.LoanModel;
import com.test.app.dao.LoansDaoImpl;
import com.test.app.exceptions.TwinoExceptionMapper;
import com.test.app.filters.ApplicationsDetector;
import com.test.app.service.BlacklistedService;
import com.test.app.service.GeoIpClient;
import com.test.app.service.LoansService;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TwinoApplication extends Application<AppConfiguration> {

    private final HibernateBundle<AppConfiguration> hibernate = new HibernateBundle<AppConfiguration>(LoanModel.class) {
        public DataSourceFactory getDataSourceFactory(AppConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    public static void main(String[] args) throws Exception {
        new TwinoApplication().run(args);
    }

    @Override
    public String getName() {
        return "TwinoApp";
    }

    @Override
    public void initialize(Bootstrap<AppConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(new MigrationsBundle<AppConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(AppConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(AppConfiguration configuration,
                    Environment environment) {

        final LoansDaoImpl dao = new LoansDaoImpl(hibernate.getSessionFactory());
        LoansService loansService = new LoansService(dao, new BlacklistedService(), new ApplicationsDetector(),
                new GeoIpClient());

        final MainResource resource = new MainResource(loansService);

        environment.jersey().getResourceConfig().register(TwinoExceptionMapper.class);
        environment.jersey().register(resource);
    }

}
