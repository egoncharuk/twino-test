package com.test.app.filters;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class ApplicationsDetector {

    private static final int MAX_ACCESSES_PER_COUNTRY_PER_TIME_DEFAULT = 1;
    private static final long MAX_ACCESS_INTERVAL_MSEC_DEFAULT = 1000L;

    private Timer timer;
    private Map<String, AtomicInteger> numAccessPerCountry = new HashMap<>(100);

    public ApplicationsDetector() {
        timer = new Timer("ApplicationsDetector reset timer");

        final long maxAccessIntervalMsec = MAX_ACCESS_INTERVAL_MSEC_DEFAULT;
        timer.scheduleAtFixedRate(new RecentAccessesTask(), 0L, maxAccessIntervalMsec);
    }

    public boolean exceedsLimit(String ipAddressString) {
        int accesses = numAccessPerCountry.getOrDefault(ipAddressString, new AtomicInteger(0)).incrementAndGet();
        return accesses > MAX_ACCESSES_PER_COUNTRY_PER_TIME_DEFAULT;
    }

    private class RecentAccessesTask extends TimerTask {
        @Override
        public void run() {
            numAccessPerCountry.clear();
        }
    }
}
