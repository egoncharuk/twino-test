package com.test.app;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.codahale.metrics.annotation.Timed;
import com.test.app.dto.LoanDto;
import com.test.app.service.LoansService;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/loans")
@Produces(MediaType.APPLICATION_JSON)
public class MainResource {

    private static final Logger log = LoggerFactory.getLogger(MainResource.class);

    private LoansService loansService;

    public MainResource(final LoansService loansService) {
        this.loansService = loansService;
    }

    @GET
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public List<LoanDto> getAll() {
        return loansService.findAll();
    }

    @GET
    @Path("{personalId}")
    @Timed
    @UnitOfWork
    @Produces(MediaType.APPLICATION_JSON)
    public List<LoanDto> getByUser(@PathParam("personalId") final String personalId) {
        return loansService.findByUser(personalId);
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    public void apply(@Context HttpServletRequest request, final LoanDto requestDto) {
        loansService.create(requestDto, getRemoteAddr(request));
    }

    protected String getRemoteAddr(HttpServletRequest request) {
        return request.getRemoteAddr();
    }

}
