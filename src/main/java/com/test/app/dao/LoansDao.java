package com.test.app.dao;/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import java.util.List;

public interface LoansDao {
    Long create(LoanModel person);

    List<LoanModel> findAll();

    List<LoanModel> findByPersonalId(String personalId);
}
