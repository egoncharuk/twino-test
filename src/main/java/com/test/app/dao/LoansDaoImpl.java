package com.test.app.dao;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class LoansDaoImpl extends AbstractDAO<LoanModel> implements LoansDao {

    public LoansDaoImpl(final SessionFactory factory) {
        super(factory);
    }

    @Override
    public synchronized Long create(final LoanModel person) {
        return persist(person).getId();
    }

    @Override
    public List<LoanModel> findAll() {
        return list(namedQuery("com.test.app.LoanModel.findAll"));
    }

    @Override
    public List<LoanModel> findByPersonalId(String personalId) {
        return list(namedQuery("com.test.app.LoanModel.findByPersonalId").setParameter("personalId", personalId));
    }
}
