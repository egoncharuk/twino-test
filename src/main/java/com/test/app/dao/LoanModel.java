package com.test.app.dao;

import javax.persistence.*;

@Entity
@Table(name = "loans")
@NamedQueries({
        @NamedQuery(
                name = "com.test.app.LoanModel.findAll",
                query = "SELECT i FROM LoanModel i"
        ),
        @NamedQuery(
                name = "com.test.app.LoanModel.findByPersonalId",
                query = "SELECT i FROM LoanModel i WHERE personalId=:personalId"
        ),
        @NamedQuery(
                name = "com.test.app.LoanModel.countByOrigin",
                query = "SELECT Count(i) FROM LoanModel i WHERE i.origin=:origin"
        )
})
public class LoanModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "loans_seq_gen")
    @SequenceGenerator(name = "loans_seq_gen", sequenceName = "loans_seq", allocationSize = 1)
    private Long id;

    private int amount;

    private String term;

    private String name;

    private String surname;

    private String personalId;

    private String origin = "lv";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoanModel)) return false;

        LoanModel loanModel = (LoanModel) o;

        if (amount != loanModel.amount) return false;
        if (id != null ? !id.equals(loanModel.id) : loanModel.id != null) return false;
        if (term != null ? !term.equals(loanModel.term) : loanModel.term != null) return false;
        if (name != null ? !name.equals(loanModel.name) : loanModel.name != null) return false;
        if (surname != null ? !surname.equals(loanModel.surname) : loanModel.surname != null) return false;
        if (personalId != null ? !personalId.equals(loanModel.personalId) : loanModel.personalId != null) return false;
        return !(origin != null ? !origin.equals(loanModel.origin) : loanModel.origin != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + amount;
        result = 31 * result + (term != null ? term.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (personalId != null ? personalId.hashCode() : 0);
        result = 31 * result + (origin != null ? origin.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LoanModel{" +
                "id=" + id +
                ", amount=" + amount +
                ", term='" + term + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", personalId='" + personalId + '\'' +
                ", origin='" + origin + '\'' +
                '}';
    }
}
