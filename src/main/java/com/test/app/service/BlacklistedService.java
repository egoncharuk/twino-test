package com.test.app.service;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BlacklistedService {

    private static final Logger log = LoggerFactory.getLogger(BlacklistedService.class);

    private Set<String> blacklistedPersonalIds = new HashSet<>();

    public BlacklistedService() {
        try {
            final InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("blacklisted.users");
            final String blacklistedStr = IOUtils.toString(resourceAsStream);
            blacklistedPersonalIds.addAll(Arrays.asList(StringUtils.split(blacklistedStr, "\n")));
            log.info("Blacklisted Service initialized successfully");
        } catch (IOException e) {
            log.error("Blacklisted Service initialization Failure " + e.getMessage(), e);
        }
    }

    public boolean isBlacklisted(final String personalIdToCheck) {
        if (StringUtils.isEmpty(personalIdToCheck)){
            log.error("Empty or null value was passed into Blacklisted Service");
            return false;
        }

        return blacklistedPersonalIds.contains(personalIdToCheck);
    }

}
