package com.test.app.service;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.test.app.dao.LoanModel;
import com.test.app.dao.LoansDao;
import com.test.app.dto.LoanDto;
import com.test.app.exceptions.BlacklistedException;
import com.test.app.exceptions.CountryExceedsLimit;
import com.test.app.filters.ApplicationsDetector;
import com.test.app.mappers.ObjectsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class LoansService {

    private static final Logger log = LoggerFactory.getLogger(LoansService.class);

    private LoansDao loansDao;

    private BlacklistedService blacklistedService;

    private ApplicationsDetector applicationsDetector;

    private GeoIpClient geoIpClient;

    public LoansService(LoansDao dao, BlacklistedService blacklistedService,
                        ApplicationsDetector applicationsDetector,
                        GeoIpClient geoIpClient) {

        this.loansDao = dao;
        this.blacklistedService = blacklistedService;
        this.geoIpClient = geoIpClient;
        this.applicationsDetector = applicationsDetector;

    }

    public void create(final LoanDto requestDto, String remoteIp) {

        final String countryOrigin = geoIpClient.getCountryOrigin(remoteIp);

        if (applicationsDetector.exceedsLimit(countryOrigin)) {
            throw new CountryExceedsLimit(countryOrigin + " exceeds limit.");
        }

        final String personalId = requestDto.getPersonalId();
        if (blacklistedService.isBlacklisted(personalId)) {
            final String msg = String.format("Person with personal id [%s] is blacklisted.", personalId);
            //log.warn(msg);
            throw new BlacklistedException(msg);
        }

        LoanModel newModel = new LoanModel();
        ObjectsMapper.mapper().map(requestDto, newModel);
        newModel.setOrigin(countryOrigin);
        loansDao.create(newModel);
    }

    public List<LoanDto> findByUser(final String personalId) {
        final List<LoanModel> all = loansDao.findByPersonalId(personalId);
        return ObjectsMapper.mapper().mapAsList(all, LoanDto.class);
    }

    public List<LoanDto> findAll() {
        final List<LoanModel> all = loansDao.findAll();
        return ObjectsMapper.mapper().mapAsList(all, LoanDto.class);
    }
}
