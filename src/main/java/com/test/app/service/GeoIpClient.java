package com.test.app.service;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.app.mappers.JsonMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeoIpClient {

    private static final Logger log = LoggerFactory.getLogger(GeoIpClient.class);

    public static final String DEFAULT_COUNTRY = "lv";

    private ObjectMapper mapper = JsonMapper.getJsonMapper();

    public String getCountryOrigin(String ipAddress) {
        String resolvedCountry = DEFAULT_COUNTRY;
        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://freegeoip.net/json/" + ipAddress);
            HttpResponse response = client.execute(request);

            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                final String json = IOUtils.toString(response.getEntity().getContent());
                final GeoResult geoResult = mapper.readValue(json, GeoResult.class);
                final String countryName = geoResult.getCountry_name();
                if (!StringUtils.isEmpty(countryName)) {
                    resolvedCountry = countryName;
                }
            }
        } catch(Exception e) {
            log.error("Error raised while detecting country origin " + e.getMessage(), e) ;
        }

        return resolvedCountry;
    }

    private static class GeoResult {
        private String country_name;

        //Introducing the dummy constructor
        public GeoResult() {
        }

        public String getCountry_name() {
            return country_name;
        }

        public void setCountry_name(String country_name) {
            this.country_name = country_name;
        }
    }
}
