package com.test.app.transport;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class JsonDynamicDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jsonparser,
                              DeserializationContext deserializationcontext) throws IOException {
        ObjectCodec oc = jsonparser.getCodec();
        JsonNode node = oc.readTree(jsonparser);
        return node.toString();
    }

}

