package com.test.app.exceptions;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import org.apache.http.HttpStatus;

public class CountryExceedsLimit extends AbstractTwinoException {
    public CountryExceedsLimit(String message) {
        super(message, HttpStatus.SC_FORBIDDEN);
    }
}
