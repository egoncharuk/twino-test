package com.test.app.exceptions;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */
 
public abstract class AbstractTwinoException extends RuntimeException {

    private int responseStatus;

    public AbstractTwinoException(final String message, int responseStatus) {
        super(message);
        this.responseStatus = responseStatus;
    }

    public int getResponseStatus() {
        return responseStatus;
    }
}
