package com.test.app.exceptions;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import org.apache.http.HttpStatus;

public class BlacklistedException extends AbstractTwinoException {

    public BlacklistedException(final String message) {
         super(message, HttpStatus.SC_FORBIDDEN);
     }

}
