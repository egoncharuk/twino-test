package com.test.app.exceptions;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.app.mappers.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TwinoExceptionMapper implements ExceptionMapper<AbstractTwinoException> {

    private static final Logger log = LoggerFactory.getLogger(TwinoExceptionMapper.class);

    private ObjectMapper jsonMapper = JsonMapper.getJsonMapper();

    @Override
    public Response toResponse(final AbstractTwinoException exception) {

        final String json;
        try {
            json = jsonMapper.writeValueAsString(exception);
        } catch (JsonProcessingException e) {
            log.error("Error while converting data json: " + e.getMessage(), e);
            return Response.serverError().build();
        }

        return Response.status(exception.getResponseStatus())
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(json)
                .build();
    }

}
