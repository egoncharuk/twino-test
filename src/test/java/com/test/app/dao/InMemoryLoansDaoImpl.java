package com.test.app.dao;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InMemoryLoansDaoImpl implements LoansDao {

    Set<LoanModel> stored = new HashSet<>();
    AtomicLong seq = new AtomicLong(0);

    @Override
    public Long create(LoanModel loan) {
        loan.setId(seq.incrementAndGet());
        stored.add(loan);
        return loan.getId();
    }

    @Override
    public List<LoanModel> findAll() {
        return new ArrayList<>(stored);
    }

    @Override
    public List<LoanModel> findByPersonalId(String personalId) {
        return stored.stream().filter(t -> t.getPersonalId().equals(personalId)).collect(Collectors.toList());
    }
}
