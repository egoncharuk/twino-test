package com.test.app;
/*
 * Copyright C.T.Co Ltd, 15/25 Jurkalnes Street, Riga LV-1046, Latvia. All rights reserved.
 */

import com.test.app.dao.InMemoryLoansDaoImpl;
import com.test.app.dto.LoanDto;
import com.test.app.filters.ApplicationsDetector;
import com.test.app.service.BlacklistedService;
import com.test.app.service.GeoIpClient;
import com.test.app.service.LoansService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MainResourceTest {

    private static final LoansService loansService = new LoansService(new InMemoryLoansDaoImpl(), new BlacklistedService(),
            new ApplicationsDetector(), new GeoIpClient());

    private static final MainResource mainResource = spy(new MainResource(loansService));

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(mainResource)
            .build();


    @Before
    public void setup() {
        doReturn("1.1.1.1").when(mainResource).getRemoteAddr(any(HttpServletRequest.class));
    }


    @Test
    public void testSaveNewLoan_Ok() {

        final GenericType<List<LoanDto>> returnListLoans = new GenericType<List<LoanDto>>() {
        };

        assertThat(getTarget().request().get(returnListLoans)).isEmpty();

        final LoanDto dto = new LoanDto();
        dto.setName("Vasily");
        dto.setPersonalId("12323-1231233");
        dto.setSurname("Pupkin");
        dto.setTerm("new term");
        dto.setAmount(1000);

        assertEquals(getTarget().request().post(Entity.json(dto)).getStatus(), 204);

        final List<LoanDto> returnList = getTarget().request().get(returnListLoans);
        assertThat(returnList).hasSize(1);
        assertEquals(returnList.get(0), dto);

        final LoanDto dto2 = new LoanDto();
        dto2.setName("Vasily2");
        dto2.setPersonalId("12323-1231235");
        dto2.setSurname("Pupkin2");
        dto2.setTerm("new term");
        dto2.setAmount(1000);

        assertEquals(getTarget().request().post(Entity.json(dto2)).getStatus(), 204);

        final List<LoanDto> returnListByPersonalId = getTargetByPersonalId("12323-1231235").request().get(returnListLoans);
        assertThat(returnListByPersonalId).hasSize(1);
        assertEquals(returnListByPersonalId.get(0), dto2);
    }

    private WebTarget getTarget() {
        return resources.client().target("/loans");
    }

    private WebTarget getTargetByPersonalId(final String personalId) {
        return resources.client().target("/loans/" + personalId);
    }

}
